package com.vhl.sms.caster.commons.model.school.advisory

data class RefAdvisoryStudent(
    val advisoryId: Int,
    val studentId: Int
)