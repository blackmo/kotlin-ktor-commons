package com.vhl.sms.caster.commons.model.base

interface UserBase {
    var id: Int
    val userName: String
    val password: String
}
