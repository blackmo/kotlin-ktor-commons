package com.vhl.sms.caster.commons.status


import com.vhl.sms.caster.commons.model.Contact
import com.vhl.sms.caster.commons.model.Message
import com.vhl.sms.caster.commons.model.MessageReceived
import com.vhl.sms.caster.commons.model.MessageToSend
import com.vhl.sms.caster.commons.status.listener.StatusListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class LocalMessageHandler {

    companion object {
        const val FAILED_TO_SEND = "FAILED_TO_END"
        const val UPDATE_SUCCESS = "UPDATE_SUCCESS"
        const val SUCCESS_SEND = "SUCCESS_SEND"
        const val MESSAGE_RECEIVED = "MESSAGE_RECEIVED"
        const val SEND_MESSAGE = "SEND_MESSAGE"
        const val RESEND_MESSAGE = "RESEND_MESSAGE"
        const val SYNC_MESSAGES = "SYNC_MESSAGES"
        const val SYNC_FAILED = "SYNC_FAILED"
        const val DISCONNECT = "DISCONNECT"
    }

    var listener: StatusListener? = null

    val sentItems: MutableMap<Int, MessageToSend> = mutableMapOf()
    val draftMessages: MutableMap<Int, MessageToSend> = mutableMapOf()
    val messageReceived: MutableList<MessageReceived> = mutableListOf()

    fun moveItemToSend(id: Int) {
        draftMessages.remove(id)?.also {
            sentItems[id] = it
        }
    }

    fun updateDraftMessageFailed(id: Int) {
        draftMessages[id]?.status = FAILED_TO_SEND
        listener?.updateOnMessageFailed(id)
    }

    fun addMessageToReceived(message: MessageReceived): String {
        messageReceived.add(message)
        listener?.notifyOnMessageReceived(message.getRecipientNumber(), message)
        return "SUCCESS"
    }

    fun createConversation(from: Contact) = runBlocking {
        withContext(Dispatchers.Default) {
            val conversation = mutableMapOf<String, Message>()

            val messageWithContact = messageReceived.filter { message ->
                message.getRecipientNumber() == from.number
            }
            messageWithContact.forEach { conversation[it.date!!] = it }
            messageReceived.removeAll(messageWithContact)

            val draftWithContact = draftMessages.filter { message ->
                message.value.getRecipientNumber() == from.number
            }
            draftWithContact.forEach { conversation[it.value.date!!] = it.value }
            draftWithContact.forEach { draftMessages.remove(it.key) }

            conversation.map { it.value }.sortedBy { it.date }
        }
    }

    suspend fun getConversation(): Map<Contact, List<Message>> {
        val mapConversation = mutableMapOf<Contact, List<Message>>()
        while (messageReceived.isNotEmpty()) {
            val firstContact = messageReceived[0].contact
            mapConversation[firstContact] = createConversation(firstContact)
        }
        return mapConversation
    }
}