package com.vhl.sms.caster.commons.model.enums

enum class Privileges {
    ADMIN,
    ADVISER,
    STUDENT
}