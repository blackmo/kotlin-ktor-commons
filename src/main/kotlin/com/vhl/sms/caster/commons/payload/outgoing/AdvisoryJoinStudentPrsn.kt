package com.vhl.sms.caster.commons.payload.outgoing

import com.vhl.sms.caster.commons.model.school.Student

data class AdvisoryJoinStudentPrsn(
    val advisoryId: Int,
    val classYear: Int,
    val classLevel: Int,
    val sectionName: String?,
    val sectionNumber: Int,
    val advisoryName: String,
    val students: List<Student>
)

