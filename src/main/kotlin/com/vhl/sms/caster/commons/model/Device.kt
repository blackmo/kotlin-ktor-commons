package com.vhl.sms.caster.commons.model

data class Device(
    val deviceId: String,
    val mobileNumber: String,
    val user: String
)