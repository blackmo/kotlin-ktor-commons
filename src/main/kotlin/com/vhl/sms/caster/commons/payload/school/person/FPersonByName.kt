package com.vhl.sms.caster.commons.payload.school.person

class FPersonByName(
    val lastName: String,
    val firstName: String? = null,
    val middleName: String? = null,
    val count: Int = 0,
    val offset: Int = 0
)