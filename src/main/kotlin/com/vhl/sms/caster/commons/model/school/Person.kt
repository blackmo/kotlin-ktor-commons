package com.vhl.sms.caster.commons.model.school

import java.time.LocalDate


data class Person(
    val id: Int,
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val gender: String?,
    val address: String,
    val mobileNo: String?,
    val birthday: LocalDate?
)

