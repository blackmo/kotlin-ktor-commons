package com.vhl.sms.caster.commons.model.school.subject.type

data class SubjectType(
    val id: Int = 0,
    val name: String,
    val description: String,
    val gradleLevel: Int
)