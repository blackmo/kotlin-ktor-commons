package com.vhl.sms.caster.commons.model

import com.vhl.sms.caster.commons.model.base.UserBase
import com.vhl.sms.caster.commons.model.enums.Privileges
import com.vhl.sms.caster.commons.model.school.Person

data class User(
    override var id: Int = 0,
    override val userName: String,
    override val password: String,
    val person: Person?,
    val privileges: Privileges
): UserBase