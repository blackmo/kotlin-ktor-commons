package com.vhl.sms.caster.commons.payload.common

import com.vhl.sms.caster.commons.model.Contact

data class ContactList(
    val contactList: List<Contact>
)

