package com.vhl.sms.caster.commons.errors

data class FailedInsertError(
    override val message: String,
    override val origin: String
): BasicErrors(message, E_FAILED_INSERT, origin)

data class AlreadyExistError(
    override val message: String,
    override val origin: String
) : BasicErrors(message, E_ALREADY_EXIST, origin)


data class InvalidTokenError(
    override val message: String = "Invalid token",
    override val origin: String
) : BasicErrors(message, E_CREDENTIALS, origin)

data class InvalidSessionError(
    override val message: String = "Invalid Mobile Session",
    override val origin: String
) : BasicErrors(message, E_SESSION, origin)


data class FailedToRetrieveDataError(
    override val message: String = "Failed to retrieved data Error",
    override val origin: String
) : BasicErrors(message, E_ALREADY_EXIST, origin)

data class DataNotFoundError(
    override val message: String = "Data not found",
    override val origin: String
) : BasicErrors(message, E_ALREADY_EXIST, origin)

data class UpdateFailedError(
    override val message: String = "Update fail error",
    override val origin: String
) : BasicErrors(message, FAILED_UPDATE, origin)

data class MissingRequiredInputsError(
    override val message: String,
    override val origin: String
) : BasicErrors(message, E_MISSING_REQUIRED_INPUT, origin)

data class InvalidRequiredInputsError(
    override val message: String = "Invalid required input",
    override val origin: String
) : BasicErrors(message, E_INVALID_INPUT, origin)

data class InvalidCredentialsError(
    override val message: String,
    override val origin: String
): BasicErrors(message, E_CREDENTIALS, origin)

open class BasicErrors(
    override val message: String,
    open val errorCode: String,
    open val origin: String
) : Exception() {
    override fun toString() =
        "${this.javaClass.simpleName}{message: $message, errorCode: $errorCode, origin: $origin}"

}


const val E_FAILED_INSERT = "E_FAILED_INSERT"
const val E_NO_SESSION = "E_NO_SESSION"
const val E_SESSION_EXPIRED = "E_SESSION_EXPIRED"
const val E_CREDENTIALS = "ERROR_INVALID_CREDENTIALS"
const val E_SESSION = "ERROR_INVALID_MOBILE_SESSION"
const val E_ALREADY_EXIST = "ERROR_ALREADY_EXIST"
const val E_MISSING_REQUIRED_INPUT = "ERROR_ALREADY_EXIST"
const val E_INVALID_INPUT = "E_INVALID_INPUT"
const val FAILED_UPDATE = "ERROR_UPDATE"
const val FAILED_DELETE = "ERROR_DELETE"
const val FAILED_ADD = "ERROR_ADD"
const val FAILED_GET = "ERROR_GET"
const val E_SQL = "SQL_ERROR"