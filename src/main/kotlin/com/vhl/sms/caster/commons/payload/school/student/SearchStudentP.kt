package com.vhl.sms.caster.commons.payload.school.student

data class SearchStudentP(
    val byLrnId: Boolean = false,
    val lastName: String? = "",
    val lrnID: String? = "",
    val classYear: Int? = 0,
    val classLevel: String? = "",
    val classSection: String? = "",
    val status: String? = ""
)