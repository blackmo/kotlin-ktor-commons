package com.vhl.sms.caster.commons.response

interface BasicResponse {
    val message: String
    val code: String
    val data: Any?
}

data class Response<T>(
    override val code: String,
    override val message: String,
    override val data: T? = null
) : BasicResponse
