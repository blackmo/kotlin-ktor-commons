package com.vhl.sms.caster.commons.model.school.advisory

import com.vhl.sms.caster.commons.model.school.Adviser

data class ClassAdvisory(
    val id: Int,
    val yearLevel: Int,
    val sectionName: String,
    val adviser: Adviser
)