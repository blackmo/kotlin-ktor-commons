package com.vhl.sms.caster.commons.constants.school.student.status

enum class StudentStatus {
    NEW,
    ENROLLED,
    GRADUATED
}