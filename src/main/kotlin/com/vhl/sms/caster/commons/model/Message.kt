package com.vhl.sms.caster.commons.model

interface Message {
    val id: Int
    val body: String
    val date: String?
    var status: String
    val contact: Contact
    var fetched: Boolean
    fun getRecipientNumber(): String
}