package com.vhl.sms.caster.commons.payload.school.adviory

data class SearchAdvisoryP(
    val classYear: Int,
    val adviserId: Int?,
    val classSection: String?,
    val classLevel: String?
)