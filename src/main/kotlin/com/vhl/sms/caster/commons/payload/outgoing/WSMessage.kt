package com.vhl.sms.caster.commons.payload.outgoing

data class WSMessage(val code: String, val message: String)