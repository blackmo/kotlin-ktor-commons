package com.vhl.sms.caster.commons.model

data class Contact(
    val id: Int? = 0,
    val name: String = "",
    val number: String,
    val userId: Int
)