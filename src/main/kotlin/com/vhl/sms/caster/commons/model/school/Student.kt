package com.vhl.sms.caster.commons.model.school

import com.vhl.sms.caster.commons.constants.school.student.status.StudentStatus

data class Student(
    val id: Int,
    val lrnID: String,
    val person: Person,
    val mother: Person?,
    val father: Person?,
    val status: String = StudentStatus.NEW.name
)