package com.vhl.sms.caster.commons.payload.school.student.batch

import com.vhl.sms.caster.commons.model.school.Student


data class BRegistrationResponse(
    val success: List<Student>,
    val failed: List<BRErrorStudentResponse>
)

data class BRErrorStudentResponse(
    val errorDesc: String,
    val student: Student
)