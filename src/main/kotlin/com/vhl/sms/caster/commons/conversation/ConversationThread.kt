package com.vhl.sms.caster.commons.conversation

import com.vhl.sms.caster.commons.model.Contact
import com.vhl.sms.caster.commons.model.Message

data class ConversationThread(val contact: Contact, val messages: List<Message>)

