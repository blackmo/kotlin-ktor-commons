package com.vhl.sms.caster.commons.payload.outgoing

import com.vhl.sms.caster.commons.model.school.Person

data class StudentJoinSoftPrsn(
    val id: Int,
    val lrnId: String,
    val person: Person
)