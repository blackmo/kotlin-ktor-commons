package com.vhl.sms.caster.commons.service

import se.simbio.encryption.Encryption

class EncryptionService(private val password: String, private val salt: String) {

    companion object {

    }

    public fun getEncryptionType(type: EncryptionType): Encryption {
        return when (type) {
            EncryptionType.DEFAULT -> defaultStringEncryption()
        }
    }

    private  fun defaultStringEncryption(): Encryption = Encryption.getDefault(password, salt, ByteArray(16))

    public enum class EncryptionType {
       DEFAULT
    }
}
