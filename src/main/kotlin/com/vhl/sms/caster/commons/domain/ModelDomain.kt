package com.vhl.sms.caster.commons.domain

import java.time.LocalDateTime


interface ModelDomain {
    var createdAt: LocalDateTime
    var modifiedAt: LocalDateTime
}