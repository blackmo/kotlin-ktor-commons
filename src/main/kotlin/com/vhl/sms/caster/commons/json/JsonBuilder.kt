package com.vhl.sms.caster.commons.json

class JsonBuilder {
    companion object {
        fun create() = JsonBuilder()
    }

    private val jsonValues = mutableMapOf<String, Any>()

    fun build(): String {
        val strVal = jsonValues.createAsList()
        return strVal.stringify()
    }

    private fun Map<String, Any>.createAsList(): List<String> {
        val list = mutableListOf<String>()
        this.mapNotNull {
            when (it.value) {
                is Array<*> -> {
                    val body = fromTypeArray(it.value as Array<*>)
                    list.add("\"${it.key}\": $body")
                }
                is JsonBuilder -> {
                    val body = (it.value as JsonBuilder).build()
                    list.add("\"${it.key}\": $body")
                }
                else -> list.add("\"${it.key}\": ${it.value}")
            }
        }
        return list
    }

    private fun List<String>.stringify(): String {
        var x = ""
        if (this.isNotEmpty()) {
            for (i in 0 until this.size) {
                x += "${this[i]},"
            }
        }
        return "{${x.sanitizeComma()}}"
    }

    private fun fromTypeArray(value: Array<*>): String {
        var x = ""
        for (i in value.indices) {
            x += when (value[i]) {
                is JsonBuilder -> "${(value[i] as JsonBuilder).build()},"
                is Array<*> -> "${fromTypeArray((value[i] as Array<*>))}, "
                else -> "${value[i]},"
            }
        }
        return "[${x.sanitizeComma()}]"
    }

    private fun String.sanitizeComma() = this.replace("""[,]$|[,\s]+$""".toRegex(), "")


    fun add(name: String, value: Double) {
        jsonValues[name] = value
    }

    fun add(name: String, value: String) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Int) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Boolean) {
        jsonValues[name] = value
    }

    fun add(name: String, value: JsonBuilder) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Double>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<String>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Int>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Boolean>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<JsonBuilder>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Array<Double>>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Array<String>>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Array<Int>>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Array<Boolean>>) {
        jsonValues[name] = value
    }

    fun add(name: String, value: Array<Array<JsonBuilder>>) {
        jsonValues[name] = value
    }

    fun value() = jsonValues
}
