package com.vhl.sms.caster.commons.status.listener

import com.vhl.sms.caster.commons.model.Message

interface StatusListener {
    fun updateOnMessageFailed(messageId: Int)

    fun notifyOnMessageReceived(contact: String, message: Message)
}