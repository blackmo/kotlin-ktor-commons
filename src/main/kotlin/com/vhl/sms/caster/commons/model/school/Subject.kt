package com.vhl.sms.caster.commons.model.school

data class Subject(
    val id: Int,
    val gradeLevel: Int,
    val subjectName: String,
    val student: Student,
    val adviser: Adviser,
    val advisory: Advisory,
    val description: String?,
    val grading1: Int?,
    val grading2: Int?,
    val grading3: Int?,
    val grading4: Int?
)