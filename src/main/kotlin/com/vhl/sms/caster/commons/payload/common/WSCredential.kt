package com.vhl.sms.caster.commons.payload.common

data class WSCredential(
    val userName: String,
    val password: String,
    val deviceId: String? = null
)