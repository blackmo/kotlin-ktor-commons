package com.vhl.sms.caster.commons.payload.common

data class ChangePassword(
    val oldPassword: String,
    val newPassword: String,
    val conPassword: String
)