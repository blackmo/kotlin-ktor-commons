package com.vhl.sms.caster.commons.model

data class MessageReceived(
    override val id: Int,
    override val body: String,
    override val date: String?,
    override var status: String,
    override val contact: Contact,
    override var fetched: Boolean = false
) : Message {
    override fun getRecipientNumber(): String = contact.number
}