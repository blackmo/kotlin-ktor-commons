package com.vhl.sms.caster.commons.payload.common

import com.vhl.sms.caster.commons.model.Contact
import com.vhl.sms.caster.commons.model.Message

data class MessageThread(
    val contact: Contact,
    val messages: List<Message>
)