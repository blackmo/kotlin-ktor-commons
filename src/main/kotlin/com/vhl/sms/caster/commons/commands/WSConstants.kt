package com.vhl.sms.caster.commons.commands

class WSConstants {
    companion object {
        const val LOGIN_WS_DEVICE = "DEVICE_LOGIN_WS::"
        const val LOGIN_WS_WEB = "WEB_LOGIN_WS::"
        const val PUSH_ALL_MESSAGES =  "PUSH_ALL_MESSAGES::"

        const val DVC_RETRIEVE_MESSAGE_TO_SEND = "DVC_RETRIEVE_MESSAGE_TO_SEND::"
    }
}
