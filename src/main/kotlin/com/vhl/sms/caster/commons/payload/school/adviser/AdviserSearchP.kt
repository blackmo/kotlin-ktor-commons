package com.vhl.sms.caster.commons.payload.school.adviser

data class AdviserSearchP(
    val lastName: String
)