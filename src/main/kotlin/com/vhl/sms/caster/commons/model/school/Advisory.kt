package com.vhl.sms.caster.commons.model.school

data class Advisory(
    val id: Int,
    val classYear: Int,
    val classLevel: Int,
    val advisoryName: String,
    val sectionName: String?,
    val sectionNumber: Int,
    val adviser: Adviser,
    val listStudent: List<Student>,
    val listLrnInString: String
)