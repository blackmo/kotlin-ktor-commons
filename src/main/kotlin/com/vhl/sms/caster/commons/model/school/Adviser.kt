package com.vhl.sms.caster.commons.model.school

data class Adviser(
    val id: Int,
    val person: Person,
    val licenseNumber: String?
)
